﻿using Library.Lab;
using Library.LabEquipment;
using Library.LabEquipment.Drivers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml;
using System.Text;
using System.Diagnostics;



namespace LibraryXrdLabEquipmentTest
{
    [TestClass]
    public class DriverTest
    {
        private static XmlNode configNode;
        private static SerialCommDriver serialCommDriver;


        [TestInitialize]
        public void DriverCreateTestSetup()
        {
// create a new XmlDocument
            string xmlStr = "<root><commType><type>serial</type><baudRate>9600</baudRate><port>COM3</port></commType></root>";
            XmlDocument xmlDoc = XmlUtilities.GetXmlDocument(xmlStr);
            Assert.IsNotNull(xmlDoc, "XmlDocument is null");
            configNode = XmlUtilities.GetXmlNode(xmlDoc, "root");
            Assert.IsNotNull(configNode, "XmlCommTypeNode is null");
            serialCommDriver = new SerialCommDriver(configNode);
            Assert.IsNotNull(serialCommDriver, "Serial connection not created");
            Assert.IsNotNull(serialCommDriver.serialPort, "Serial Comm driver is null");
            Assert.IsFalse(serialCommDriver.IsOpen, "Serial Comm driver is opened");
            serialCommDriver.Open();
            Assert.IsTrue(serialCommDriver.IsOpen, "Serial Comm driver is not opened");
        }

        [TestMethod]
        public void SendDataTest()
        {
            string sendDataString = "This is test data";
            byte[] sendData = Encoding.ASCII.GetBytes(sendDataString);
            Assert.IsNotNull(sendData);
            bool success = serialCommDriver.SendData(sendData, sendDataString.Length);
            Assert.IsTrue(success, "Data was not sent to the serial port");
        }

        [TestMethod]
        public void ReadDataTest()
        {
            string xmlStr = "<root><commType><type>serial</type><baudRate>9600</baudRate><port>COM4</port></commType></root>";
            XmlDocument xmlDoc = XmlUtilities.GetXmlDocument(xmlStr);
            Assert.IsNotNull(xmlDoc, "XmlDocument is null");
            configNode = XmlUtilities.GetXmlNode(xmlDoc, "root");
            SerialCommDriver s = new SerialCommDriver(configNode);
            s.Open();
            string sendToCommFour = "my test message";
            byte[] sendToFourData = Encoding.ASCII.GetBytes( sendToCommFour );
            s.SendData(sendToFourData, sendToCommFour.Length); 

            byte[] bytearray = serialCommDriver.ReadData(100);
            string s3 = Encoding.ASCII.GetString(bytearray);
            Debug.Write(s3);
            Assert.AreNotEqual( 0, s3.Length, "zero lenngth output");
        }
    }
}
