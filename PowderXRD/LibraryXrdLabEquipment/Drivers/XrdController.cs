﻿using System;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using Library.Lab;
using Library.LabEquipment.Engine;
using Library.LabEquipment.Settings;
using System.Threading;

namespace Library.LabEquipment.Drivers
{
    public class XrdController
    {
        #region Class constants and variables
        private const string STRLOG_ClassName = "XrdController";

        //
        // String Constants for Log File
        //

        private const string STRLOG_uninitialized = "Not Initialized !";
        
        //
        // String Constant for preparation of serial commands.
        // 
        private const string STRCMD_start = "\x02";
        private const string STRCMD_end = "\x0D\x0A";
        private const string STRCMD_stop = "SP";
        private const string STRCMD_datum = "DM";
        private const string STRCMD_position = "MV";
        private const string STRCMD_ftMeasurement = "FT";
        private const string STRCMD_hvSet = "HV";
        private const string STRCMD_phaSet = "PH";
        private const string STRCMD_xrayShutter = "XS";
        private const string STRCMD_plotter = "PL";
        private const string STRCMD_ptcControl = "PT";
        private const string STRCMD_angleRead = "AR";
        private const string STRCMD_measure = "M1";
        private const string STRCMD_pfMeasure = "M2";
        private const string STRCMD_fsMeasure = "M3";
        private const string STRCMD_ptcMeasure = "M4";
        private const string STRCMD_selfTest = "ST";
        private const string STRCMD_setting1 = "S1"; // angle
        private const string STRCMD_setting2 = "S2"; // goniometer peak position
        private const string STRCMD_setting3 = "S3";
        private const string STRCMD_calibrate = "CL";
        private const string STRCMD_comma = ",";
        private const string STRCMD_space = " ";
        private const string STRCMD_null = "\x00";
        private const string STRCMD_xrdScanParameters = "";

        //
        // String constants for exception messages
        //

        //
        // String constants for error messages
        //

        //
        // String constants for log messages
        //

        //
        // Constant values
        //

        private const int DEFAULT_TIMEOUT = 5000;
        private const int INTENSITY_DATA_LENGTH = 8;

        //
        // Local variables
        //
        

        protected bool isXrayShutterOpen;
        private XrdAxis xrdAxis;
        private XrdLrSpecification xrdLrSpecification;
        private XrdUnit xrdUnit;
        private XrdScanType xrdScanType;
        private XrdHvSpecification xrdHvSpecification;
        private int xrdVoltage;
        private int xrdBaseline;
        private int xrdWindow;
        private SerialCommDriver serialCommDriver;

        private string readBuffer;

        #region properties
        protected bool online;
        protected string statusMessage;
        protected string dataVector = string.Empty;
        protected object dataVectorLock;
        protected object serialPortLock;
        protected object onlineLock;

        public string DataVector
        {
            get {
                string tempDataVector = string.Empty;

                lock (dataVectorLock)
                {
                    tempDataVector = dataVector;
                }

                return tempDataVector;
            }
        }

        public bool Online
        {
            get { return online; }
        }

        public string StatusMessage
        {
            get { return statusMessage; }
        }

        #endregion

        //private enum XrdOutputType
        //{
        //    XRD_INTENSITY,
        //    XRD_INITIALVALUE,
        //    XRD_COMPLETION,
        //    XRD_ERROR,
        //    XRD_NONE
        //};

        //private enum XrdResponseReadStage
        //{
        //    XRD_RESPONSE_ERROR,
        //    XRD_RESPONSE_STARTANGLE,
        //    XRD_RESPONSE_ENDANGLE,
        //    XRD_RESPONSE_LRSPECIFICATION,
        //    XRD_RESPONSE_SAMPLINGINVERVAL,
        //    XRD_RESPONSE_INTENSITY,
        //    XRD_RESPONSE_NONE
        //}

        //private struct XrdResponse
        //{
        //    public double startAngle;
        //    public double endAngle;
        //    public double currentAngle;
        //    public XrdLrSpecification lrSpecification;
        //    public double samplingInterval;
        //    public List<int> intensityData;
        //    public int errorCode;
        //    public bool isCompleted;
        //}

        //XrdResponse xrdResponse;

        #endregion

        protected string lastError;

        public XrdController(XmlNode xmlNodeEquipmentConfig, SerialCommDriver serialCommDriver)
        {
            const string STRLOG_MethodName = "XrdController";
            //
            // Attach serial communication device.
            //
            this.serialCommDriver = serialCommDriver;
            this.dataVectorLock = new object();
            this.serialPortLock = new object();

            Logfile.WriteCalled(null, STRLOG_MethodName);

        }

        public string GetLastError()
        {
            string lastError = this.lastError;
            this.lastError = string.Empty;
            return lastError;
        }


        public bool Initialise()
        {
            //
            //set isXrayShutterOpen to false
            // 
            bool success = true;
            isXrayShutterOpen = false;

            //
            // set other default values. use hard coded values right now.
            //
            xrdLrSpecification = XrdLrSpecification.RIGHT;
            xrdUnit = XrdUnit.GONIOMETER;
            xrdScanType = XrdScanType.STEP;
            xrdVoltage = 5000;
            xrdBaseline = 1;
            xrdAxis = XrdAxis.TWOTHETA_THETA;
            xrdHvSpecification = XrdHvSpecification.HV1;
            xrdWindow = 0;


            this.online = true;
            this.statusMessage = StatusCodes.Ready.ToString();

            //
            // Open the serial port for communication, if not open yet.
            //

            return success;
        }

        public bool ScanOnThread(double startAngle, double endAngle, double stepWidth, double samplingTime, int waitTime)
        {
            Thread thread = new Thread(() => Scan(startAngle, endAngle, stepWidth, samplingTime, waitTime));
            thread.Start();
            return true;
        }

        public bool Scan(double startAngle, double endAngle, double stepWidth, double samplingTime, int waitTime)
        {
            return Scan(xrdLrSpecification, xrdScanType, 1, 1, xrdAxis, startAngle, endAngle, 60, stepWidth, samplingTime, 5000,
                 XrdCpsCount.COUNT_LINEAR, 0, 0, 0, 0.0, 0, 1.5406, 0, 1, "Z0000000", waitTime);
        }

        public bool Scan(XrdLrSpecification lrSpecification, XrdScanType scanType, int mode2, int mode3,
            XrdAxis axis, double startAngle, double endAngle, double scanSpeed, double stepWidth, double ftTime,
            Int32 fullscale, XrdCpsCount cpsCount, int smoothingPoints, int diffPoints, int peakHeight, double peakWidth,
            int bgRemoval, double waveLength, int output, int dataTransfer, String filename, int waitTime)
        {

            String strCommand = string.Empty;
            bool success = false;

            // Logfile.Write("Wait Time in XRD : " + waitTime);
            
            strCommand = STRCMD_start + STRCMD_measure
                + STRCMD_space + lrSpecification.ToString("d")
                + STRCMD_space + scanType.ToString("d")
                + STRCMD_space + mode2.ToString("d")
                + STRCMD_space + mode3.ToString()
                + STRCMD_space + axis.ToString("d")
                + STRCMD_space + startAngle.ToString()
                + STRCMD_space + endAngle.ToString()
                + STRCMD_space + scanSpeed.ToString()
                + STRCMD_space + stepWidth.ToString()
                + STRCMD_space + ftTime.ToString()
                + STRCMD_space + fullscale.ToString()
                + STRCMD_space + cpsCount.ToString("d")
                + STRCMD_space + smoothingPoints.ToString()
                + STRCMD_space + diffPoints.ToString()
                + STRCMD_space + peakHeight.ToString()
                + STRCMD_space + peakWidth.ToString()
                + STRCMD_space + bgRemoval.ToString()
                + STRCMD_space + waveLength.ToString()
                + STRCMD_space + output.ToString()
                + STRCMD_space + dataTransfer.ToString()
                + STRCMD_space + filename.ToString()
                + STRCMD_end;


            ClearDataVector(); // TODO clear the buffer as well.
            sendCommand(strCommand, 30*1000);
            success = getIntensityData();

            return success;
        }

        public bool SetDatum()
        {
            return SetDatum(this.xrdAxis);
        }


        public virtual bool SetDatum(XrdAxis axis)
        {
            if (XrdAxis.THETA == axis || XrdAxis.TWOTHETA == axis)
            {
                _datum(axis);
                Calibrate(0.0, axis);
            }
            else if (XrdAxis.TWOTHETA_THETA == axis)
            {
                _datum(XrdAxis.THETA);
                _datum(XrdAxis.TWOTHETA);
                Calibrate(0.0, XrdAxis.THETA);
                Calibrate(0.0, XrdAxis.TWOTHETA);
            }
            return true;
        }


        public virtual bool Calibrate(double offset, XrdAxis axis)
        {
            String strCommand = "";

            strCommand = STRCMD_start + STRCMD_calibrate
                + STRCMD_space + xrdLrSpecification.ToString("d")
                + STRCMD_space + axis.ToString("d")
                + STRCMD_space + offset
                + STRCMD_end;

            sendCommand(strCommand);
            return getError();
        }


        private bool _datum(XrdAxis axis)
        {
            String strCommand = "";

            strCommand = STRCMD_start + STRCMD_datum
                + STRCMD_space + xrdLrSpecification.ToString("d")
                + STRCMD_space + xrdUnit.ToString("d")
                + STRCMD_space + axis.ToString("d")
                + STRCMD_end;

            sendCommand(strCommand);
            return getError();
        }



        public virtual bool SetAngle(XrdAxis axis, double angle)
        {
            string strCommand = "";

            strCommand = STRCMD_start + STRCMD_position
                + STRCMD_space + xrdLrSpecification.ToString("d")
                + STRCMD_space + xrdUnit.ToString("d")
                + STRCMD_space + axis.ToString("d")
                + STRCMD_space + angle + STRCMD_end;

            sendCommand(strCommand);
            return getError();
        }

        public bool SetAngle(double angle)
        {
            return this.SetAngle(xrdAxis, angle);
        }


        public bool GetAngle( out double angle )
        {
            bool success = false;
            success = GetAngle(xrdAxis, out angle);
            return success;
        }

        public bool GetAngle(XrdAxis axis, out double angle)
        {
            string strCommand = "";

            strCommand = STRCMD_start + STRCMD_angleRead
                + STRCMD_space + xrdLrSpecification.ToString("d")
                + STRCMD_space + xrdUnit.ToString("d")
                + STRCMD_space + axis.ToString("d");

            sendCommand(strCommand);

            angle = getCurrentAngle() ;

            if ( angle == -1)
            {
                return getError();
            }

            return true; 
        }



        public virtual bool SetPHA()
        {
            String strCommand = "";

            strCommand = STRCMD_start + STRCMD_phaSet
                + STRCMD_space + xrdLrSpecification
                + STRCMD_space + xrdBaseline
                + STRCMD_space + xrdWindow
                + STRCMD_end;

            sendCommand(strCommand);
            return getError();
        }

        public virtual bool SetHV()
        {
            String strCommand = "";

            strCommand = STRCMD_start + STRCMD_hvSet
                + STRCMD_space + xrdLrSpecification
                + STRCMD_space + xrdHvSpecification
                + STRCMD_space + xrdVoltage
                + STRCMD_end;

            sendCommand(strCommand);
            return getError();
        }

        public virtual bool SetXrayShutterOpen()
        {
            String strCommand = "";

            strCommand = STRCMD_start + STRCMD_xrayShutter + STRCMD_space + xrdLrSpecification.ToString("d") + STRCMD_space + 1 + STRCMD_end;
            sendCommand(strCommand);
            return getError();
        }

        public virtual bool SetXrayShutterClose()
        {
            String strCommand = "";

            strCommand = STRCMD_start + STRCMD_xrayShutter + STRCMD_space + xrdLrSpecification.ToString("d") + STRCMD_space + 0 + STRCMD_end;
            sendCommand(strCommand);
            return getError();
        }

        public virtual bool ClearError()
        {
            return true;
        }

        public bool OpenSerialPort()
        {
            if (serialCommDriver.IsOpen == false)
            {
                serialCommDriver.Open();
            }

            return serialCommDriver.IsOpen;
        }

        public bool CloseSerialPort()
        {
            if (serialCommDriver.IsOpen == true)
            {
                serialCommDriver.Close();
            }

            return !serialCommDriver.IsOpen;
        }

        //
        // Internal functions
        //

        private void updateBufferWithSerialData(int timeout = 0)
        {
            readBuffer = serialCommDriver.ReadFromSerial(timeout);
            Logfile.Write("DEBUG : Data read from serial Port \n" + readBuffer);
        }

        private void sendCommand(string command, int timeout = DEFAULT_TIMEOUT )
        {
            Logfile.Write("DEBUG Sending command : " + command);
            lock (serialPortLock)
            {
                updateBufferWithSerialData();
                serialCommDriver.WriteToSerial(command);
                updateBufferWithSerialData(timeout);
            }
         }


        private bool getIntensityData()
        {
            int length = readBuffer.Length;
            int errorIndex, initValueIndex, completionIndex, intensityIndex, index;
            List<int> intensityDataList = new List<int>();

            Logfile.Write("Length of read data is " + length.ToString());

            // read till s or e is found
            // if e process a error.
            // else s1 -> get intensity data.


            if ( length < 1 )
            {
                return false;
            }

            // find the index of first null character in the string.
            int firstNullIndex = readBuffer.IndexOf('\x00');
            Logfile.Write("First Null Index: " + firstNullIndex.ToString());

            if (firstNullIndex == -1 || firstNullIndex > (length - 1))
            {
                return false;
            }

            errorIndex = readBuffer.IndexOf('E', firstNullIndex);
            Logfile.Write("Error Index: " + errorIndex.ToString());

            if (errorIndex != -1)
            {
                // error found, handle for error.
                getError();
                return false;
            }

            initValueIndex = readBuffer.IndexOf('S', firstNullIndex);
            Logfile.Write("Initial Value Index: " + initValueIndex.ToString());

            if (initValueIndex == -1)
            {
                this.statusMessage = "";
                // No initial value character found, return.
                return false;
            }

            // find completion character index
            completionIndex = readBuffer.IndexOf('C', initValueIndex);
            Logfile.Write("Completion Index: " + completionIndex.ToString());

            if (completionIndex == -1)
            {
                return false;
            }

            index = initValueIndex;

            while (index < length)
            {
                string intensity;
                intensityIndex = readBuffer.IndexOf('D', index);

                if (intensityIndex == -1)
                    break;

                // skip 3 chars and grab intensity data

                index = intensityIndex + 3;
                intensity = readBuffer.Substring(index, INTENSITY_DATA_LENGTH);
                Logfile.Write("Intensity : " + intensity);

                intensityDataList.Add(Convert.ToInt32(intensity));
            }


            string tempDataVector = string.Empty;
            bool firstIteration = true;

            foreach (int intensityData in intensityDataList)
            {

                // TODO : kick out the if :)
                if (firstIteration == true)
                {
                    firstIteration = false;
                }
                else
                {
                    tempDataVector += ",";
                }

                tempDataVector += intensityData.ToString();
            }

            lock (dataVectorLock)
            {
                dataVector = tempDataVector;
            }

            Logfile.Write("DataVector " + dataVector);

            return true;
        }

        private bool getError(int errorIndex = -1)
        {
            if (readBuffer.Length == 0)
            {
                return false;
            }

            if (errorIndex == -1 && (errorIndex = readBuffer.IndexOf('E')) == -1)
            {
                return true;
            }

            string errorCode = readBuffer.Substring(errorIndex + 1, 3);
            statusMessage = "Error : " + errorCode;

            return false;
        }

        private double getCurrentAngle()
        {
            throw new NotImplementedException();
        }


        public bool DataVectorReady()
        {
            bool success = false;

            lock (dataVectorLock)
            {
                if (this.dataVector.Length != 0)
                {
                    success = true;
                }
            }

            return success;
        }

        public bool ClearDataVector()
        {
            lock (dataVectorLock)
            {
                if (this.dataVector.Length != 0)
                {
                    this.dataVector = string.Empty;
                }
            }

            return true;
        }
    }
}