﻿using System;
using System.IO;
using System.IO.Ports;
using System.Xml;
using Library.Lab;
using Library.LabEquipment;
using System.Text;

namespace Library.LabEquipment.Drivers
{
    public class SerialCommDriver : CommDriver
    {
        #region class constants and variables

        private const string STRLOG_openingSerialPort = "Opening Serial Port : ";
        
        public SerialPort serialPort;
        
        #endregion

        #region properties

        private bool timedOut;

        public bool TimedOut
        {
            get { return timedOut; }
            set { timedOut = value; }
        }

        public bool IsOpen
        {
            get
            {
                if (serialPort == null)
                {
                    return false;
                }
                else
                {
                    return serialPort.IsOpen;
                }
            }
        }

        #endregion

        //
        // Set up SerialCommDriver based on the Equipment Configuration
        //

        public SerialCommDriver(XmlNode XmlNodeEquipmentConfig)
        {
            const String STRLOG_MethodName = "SerialCommDriver";
            Logfile.WriteCalled(null, STRLOG_MethodName);

            try
            {
                XmlNode commParams = XmlUtilities.GetXmlNode(XmlNodeEquipmentConfig, Consts.STRXML_commParams, false);
                // XmlNode type = XmlUtilities.GetXmlNode(commParams, Consts.STRXML_type, false);
                string portName = XmlUtilities.GetXmlValue(commParams, Consts.STRXML_port, false);
                int baudRate = XmlUtilities.GetIntValue(commParams, Consts.STRXML_baud, 9600);
                int readTimeOut = XmlUtilities.GetIntValue(commParams, Consts.STRXML_readTimeOut, 5000);
                int writeTimeOut = XmlUtilities.GetIntValue(commParams, Consts.STRXML_writeTimeOut, 3000);

                Logfile.Write( STRLOG_openingSerialPort + portName);
                
                //
                // Create a new serial port object.
                //
                this.serialPort = new SerialPort(portName, baudRate);

                //
                //  setup the serial port parameters here.
                //
                this.serialPort.ReadTimeout = readTimeOut;
                this.serialPort.WriteTimeout = writeTimeOut;
            }
            catch(Exception ex)
            {
                throw ex;
                // in case of exception print a error log and exit.
            }

            Logfile.WriteCompleted(null, STRLOG_MethodName);
        }

        public override bool Open()
        {
            Logfile.Write("Opening serial port");

            try
            {
                if (( serialPort != null) && !IsOpen)
                {
                       serialPort.Open();
                }
            }
            catch(Exception ex)
            {
                Logfile.Write(ex.Message);
            }

            return IsOpen;
        }

        public override bool Close()
        {
            Logfile.Write("Closing serial port");

            try
            {
                if (IsOpen)
                {
                    this.serialPort.Close();
                }
            }
            catch( Exception ex )
            {
                Logfile.Write(ex.Message);
            }

            return !IsOpen;
        }

        public override bool WriteToSerial(string dataString)
        {

            bool success = false;

            try
            {
                if (dataString == null || dataString.Length == 0)
                {
                    throw new ArgumentException();
                }

                serialPort.Write(dataString);
                success = true;
            }

            catch( Exception ex )
            {
                Logfile.Write(ex.Message);
            }

            return success;
        }

        public override string ReadFromSerial(int timeOut = 0)
        {
            int timeRemaining = timeOut;
            string outputString = "";
            string tempString = "";
            // bool dataRead = false;
            int waitTimeAfterRead = 10000;

            try
            {
                do
                {
                    tempString = serialPort.ReadExisting();

                    if (tempString.Length == 0)
                    {
                        timeRemaining -= 1000;
                        System.Threading.Thread.Sleep(1000);
                        Logfile.Write("[DATAREAD_DEBUG] Sleep for 1s @ " + DateTime.Now + " : Remaining time " + timeRemaining + "ms" ); 
                    }
                    else
                    {
                        // reset the time remaining value.
                        if (timeOut != timeRemaining)
                        {
                            timeRemaining = waitTimeAfterRead;
                        }

                        Logfile.Write("[DATAREAD_DEBUG] Data Read :" + tempString);
                        outputString += tempString;
                    }
                } while (timeRemaining > 0);
            }
            catch (Exception ex)
            {
                Logfile.Write(ex.Message);
            }

            return outputString;
        }


        public override bool SendData(byte[] buffer, int bytesToSend)
        {
            bool success = false;

            try
            {
                this.serialPort.Write(buffer, 0, bytesToSend);
                success = true;
            }
            catch (Exception ex)
            {
                Logfile.Write(ex.Message);
            }

            return success;
        }

        public override byte[] ReadData(int bytesToRead)
        {
            byte[] readbuffer = new byte[bytesToRead];
            bool success = false;

            try
            {
                this.serialPort.Read(readbuffer, 0, bytesToRead);
                success = true;
            }
            catch (Exception ex)
            {
                Logfile.Write(ex.Message);
            }

            if (success)
            {
                return readbuffer;
            }

            return null;
        }

        public string ReadDataAsString(bool retry)
        {

            bool success = false;
            int retries = 25;
            string readString = "";

            if (retry == false)
            {
                retries = 1;
            }

            try
            {
                while (retries > 0)
                {
                    string temp = this.serialPort.ReadExisting();

                    if (temp.Length == 0)
                    {
                        retries--;
                        // System.Threading.Thread.Sleep(1000);
                        // Logfile.Write("Timer starts");
                        System.Threading.Thread.Sleep(1000);
                        // Logfile.Write("Timer ends");
                        continue;
                    }
                    else if (retries < 10)
                    {
                        retries = 10;
                    }

                    readString += temp;
                }

                success = true;
            }
            catch (Exception ex)
            {
                Logfile.Write(ex.Message);
            }

            return readString;
        }
    }
}
