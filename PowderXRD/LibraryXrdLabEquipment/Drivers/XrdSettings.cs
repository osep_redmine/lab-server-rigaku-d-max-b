﻿using System;

namespace Library.LabEquipment.Settings
{
    [Flags]
    public enum XrdScanType
    {
        CONTINOUS = 1,
        STEP
    }

    [Flags]
    public enum XrdAxis
    {
        TWOTHETA_THETA = 1,
        TWOTHETA,
        THETA
    }

    [Flags]
    public enum XrdLrSpecification
    {
        RIGHT = 1,
        LEFT
    }

    [Flags]
    public enum XrdCpsCount
    {
        CPS_LINEAR = 1,
        COUNT_LINEAR,
        CPS_LOG,
        COUNT_LOG
    }

    [Flags]
    public enum XrdHvSpecification
    {
        HV1 = 1,
        HV2
    }

    [Flags]
    public enum XrdUnit 
    { 
        GONIOMETER = 1,
        POLEFIGURE,
        FIBER_SPECIMEN
    }

}
