﻿using System;
using System.IO;
using System.IO.Ports;
using System.Xml;
using Library.Lab;

namespace Library.LabEquipment.Drivers
{
    public abstract class CommDriver
    {
        public abstract bool Open();
        public abstract bool Close();
        public abstract bool SendData(byte[] buffer, int bytesToSend);
        public abstract byte[] ReadData(int bytesToRead);
        public abstract bool WriteToSerial(string dataString);
        public abstract string ReadFromSerial( int timeOut );
    }
}
