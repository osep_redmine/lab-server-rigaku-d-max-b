﻿using System;
using System.Web;
using System.Xml;
using Library.Lab;
using Library.LabEquipment.Engine;
using Library.LabEquipment.Settings;
using Library.LabEquipment.Drivers;

namespace Library.LabEquipment
{
    public class EquipmentEngine : LabEquipmentEngine
    {
        #region Class Constants and Variables

        private const string STRLOG_ClassName = "EquipmentEngine";

        //
        // Constants
        //

        //
        // String constants
        //

        //
        // String constants for logfile messages
        //

        //
        // String constants for error messages
        //
        // Errors.
        public const string STRERR_noSerialCommFound = "Serial communication protocol is not specified";

        //
        // Serial LCD types - the type being used is specified in the application's configuration file
        //
        private enum CommunicationTypes
        {
            Network, Serial, None
        }

        //
        // Radiation counter types - the type being used is specified in the application's configuration file
        //
        //private enum RadiationCounterTypes
        //{
        //    ST360, Physics
        //}

        //
        // Local variables
        //
        //private FlexMotion flexMotion;
        //private CommunicationTypes serialLcdCommType;
        //private SerialLcdSer serialLcdSer;
        //private SerialLcdTcp serialLcdTcp;
        //private SerialLcdNone serialLcdNone;
        //private SerialLcd serialLcd;
        //private RadiationCounterTypes radiationCounterType;
        //private CommunicationTypes st360CounterCommType;
        //private ST360CounterSer st360CounterSer;
        //private ST360CounterTcp st360CounterTcp;
        //private ST360CounterNone st360CounterNone;
        //private ST360Counter st360Counter;
        //private PhysicsCounter physicsCounter;

        private CommunicationTypes SerialCommType;
        private XrdController xrdController;
        private SerialCommDriver serialCommDriver;

        #endregion

        //-------------------------------------------------------------------------------------------------//

        public EquipmentEngine(string rootFilePath)
            : base(rootFilePath)
        {
            const string STRLOG_MethodName = "EquipmentEngine";

            Logfile.WriteCalled(null, STRLOG_MethodName);

            int initialiseDelay = 0;
            bool hardwarePresent = true;

            try
            {
                //
                // Determine if hardware is to be used or whether this is running without hardware
                // for testing and debugging
                //
                try
                {
                    hardwarePresent = XmlUtilities.GetBoolValue(this.xmlNodeEquipmentConfig, Consts.STRXML_hardwarePresent, false);
                }
                catch
                {
                    // Key not found or invalid so assume hardware is present
                }

                if (hardwarePresent == true)
                {

                    XmlNode SerialCommParams = XmlUtilities.GetXmlNode(this.xmlNodeEquipmentConfig, Consts.STRXML_commParams);
                    String serialCommType = XmlUtilities.GetXmlValue(SerialCommParams, Consts.STRXML_type, false);

                    if (serialCommType == Consts.STRXML_serial)
                    {
                        this.serialCommDriver = new SerialCommDriver(this.xmlNodeEquipmentConfig);
                    }
                    else
                    {
                        throw new Exception(STRERR_noSerialCommFound);
                    }


                    string xrdController = XmlUtilities.GetXmlValue(this.xmlNodeEquipmentConfig, Consts.STRXML_xrdController, false);

                    if (xrdController == Consts.STRXML_dmaxRigakuB)
                    {
                        this.xrdController = new XrdController(xmlNodeEquipmentConfig, serialCommDriver);
                    }
                }
            }
            catch (Exception ex)
            {
                //
                // Log the message and throw the exception back to the caller
                //
                Logfile.WriteError(ex.Message);
                throw;
            }

            Logfile.WriteCompleted(null, STRLOG_MethodName);
        }

        //---------------------------------------------------------------------------------------//

        /// <summary>
        /// Enable power to the external equipment.
        /// </summary>
        /// <returns>True if successful.</returns>
        public override bool PowerupEquipment()
        {
            const string STRLOG_MethodName = "PowerupEquipment";
            Logfile.WriteCalled(STRLOG_ClassName, STRLOG_MethodName);
            bool success = true;

            // no need to power up, we will operate under the assumption its power up.

            string logMessage = STRLOG_Success + success.ToString();
            Logfile.WriteCompleted(STRLOG_ClassName, STRLOG_MethodName, logMessage);

            return success;
        }

        //---------------------------------------------------------------------------------------//

        /// <summary>
        /// Initialise the equipment after it has been powered up.
        /// </summary>
        /// <returns>True if successful.</returns>
        public override bool InitialiseEquipment()
        {
            const string STRLOG_MethodName = "InitialiseEquipment";

            Logfile.WriteCalled(STRLOG_ClassName, STRLOG_MethodName);

            bool success = false;

            try
            {
                // serial port get opened inside xrdController.Initialise();
                success = xrdController.Initialise();
            }
            catch (Exception ex)
            {
                Logfile.WriteError(ex.Message);
            }

            string logMessage = STRLOG_Success + success.ToString();

            Logfile.WriteCompleted(STRLOG_ClassName, STRLOG_MethodName, logMessage);

            return success;
        }

        //---------------------------------------------------------------------------------------//

        /// <summary>
        /// Disable power to the external equipment.
        /// </summary>
        /// <returns>True if successful.</returns>
        public override bool PowerdownEquipment()
        {
            const string STRLOG_MethodName = "PowerdownEquipment";

            Logfile.WriteCalled(STRLOG_ClassName, STRLOG_MethodName);

            bool success = true;

#if NO_POWERDOWN
#else
            success = this.xrdController.CloseSerialPort();
#endif

            string logMessage = STRLOG_Success + success.ToString();

            Logfile.WriteCompleted(STRLOG_ClassName, STRLOG_MethodName, logMessage);

            return success;
        }

        //-------------------------------------------------------------------------------------------------//

        public override LabStatus GetLabEquipmentStatus()
        {
            const string STRLOG_MethodName = "GetLabEquipmentStatus";

            Logfile.WriteCalled(STRLOG_ClassName, STRLOG_MethodName);

            LabStatus labStatus = new LabStatus(this.xrdController.Online, this.xrdController.StatusMessage);
            if (labStatus.online == true)
            {
                labStatus.online = this.slOnline;
                labStatus.labStatusMessage = this.slStatusMessage;
            }

            string logMessage = STRLOG_Online + labStatus.online.ToString() +
                Logfile.STRLOG_Spacer + STRLOG_StatusMessage + Logfile.STRLOG_Quote + labStatus.labStatusMessage + Logfile.STRLOG_Quote;

            Logfile.WriteCompleted(STRLOG_ClassName, STRLOG_MethodName, logMessage);

            return labStatus;
        }

        public override ExecuteCommandInfo ProcessCommand(ExecuteCommandInfo executeCommandInfo)
        {
            const string STRLOG_MethodName = "ProcessCommand";
            Logfile.WriteCalled(STRLOG_ClassName, STRLOG_MethodName);

            CommandInfo commandInfo = (CommandInfo)executeCommandInfo;
            bool success = true;
            string errorMessage = null;

            try
            {
                // try to process the execute command.
                ExecuteCommands executeCommand = (ExecuteCommands) commandInfo.command;

                switch (executeCommand)
                {
                    case ExecuteCommands.SetCurrentAngle:
                        float angle = (float)commandInfo.parameters[0];
                        success = this.xrdController.SetAngle(angle);

                        if ( success == false)
                        {
                            errorMessage = this.xrdController.GetLastError();
                        }
                        break;

                    case ExecuteCommands.GetCurrentAngle:
                        break;
                    case ExecuteCommands.SetDatum:
                        // no parameter are needed here right now;
                        if (commandInfo.parameters[0] == null)
                        {
                            XrdAxis axis = (XrdAxis)commandInfo.parameters[0];
                            success = this.xrdController.SetDatum( axis );
                        }
                        else
                        {
                            success = this.xrdController.SetDatum();
                        }

                        if (success == false)
                        {
                            errorMessage = this.xrdController.GetLastError();
                        }

                        break;
                    case ExecuteCommands.SimpleScan:
                        double startAngle = (double)commandInfo.parameters[0];
                        double endAngle = (double)commandInfo.parameters[1];
                        double stepWidth = (double)commandInfo.parameters[2];
                        double samplingTime = (double)commandInfo.parameters[3];
                        int waitTime = (int)commandInfo.parameters[4];
                        
                        // success = this.xrdController.Scan(startAngle, endAngle, stepWidth, samplingTime, waitTime);
                        Logfile.Write("Calling xrdController.ScanOnThread ");
                        success = this.xrdController.ScanOnThread(startAngle, endAngle, stepWidth, samplingTime, waitTime);
                        Logfile.Write("Output xrdController.ScanOnThread : "+ success.ToString());


                        if (success == false)
                        {
                            errorMessage = this.xrdController.GetLastError();
                            Logfile.Write(errorMessage + " FAILED");
                        }

                        break;

                    case ExecuteCommands.GetDataVectorReadyStatus:
                        success = this.xrdController.DataVectorReady();
                        break;

                    case ExecuteCommands.GetDataVector:
                        string dataVector = this.xrdController.DataVector;

                        if (dataVector.Length != 0)
                        {
                            executeCommandInfo.results = new object[] { dataVector };
                            success = true;
                        }
                        else
                        {
                            executeCommandInfo.results = new object[] { string.Empty };
                            success = false;
                        }

                        break;

                    case ExecuteCommands.SetXrayShutterOpen:
                        success = this.xrdController.SetXrayShutterOpen();

                        if (success == false)
                        {
                            errorMessage = this.xrdController.GetLastError();
                            Logfile.Write(errorMessage + " FAILED");
                        }

                        break;

                    case ExecuteCommands.SetXrayShutterClose:
                        success = this.xrdController.SetXrayShutterClose();

                        if (success == false)
                        {
                            errorMessage = this.xrdController.GetLastError();
                            Logfile.Write(errorMessage + " FAILED");
                        }

                        break;

                    case ExecuteCommands.SetSerialPortOpen:
                        success = this.xrdController.OpenSerialPort();

                         if (success == false)
                        {
                            errorMessage = this.xrdController.GetLastError();
                            Logfile.Write(errorMessage + " FAILED");
                        }

                        break;

                    case ExecuteCommands.SetSerialPortClose:
                        success = this.xrdController.CloseSerialPort();

                        if (success == false)
                        {
                            errorMessage = this.xrdController.GetLastError();
                            Logfile.Write(errorMessage + " FAILED");
                        }

                        break;



                }
            }
            catch( Exception ex )
            {
                success = false;
                errorMessage = ex.Message;
                Logfile.Write(ex.StackTrace);
                Logfile.Write(errorMessage + " EXCEPTION");
            }

                // Update success of command execution

                executeCommandInfo.success = success;

                string logMessage = STRLOG_Success + success.ToString();
                if (success == false)
                {
                    executeCommandInfo.errorMessage = errorMessage;
                    logMessage += Logfile.STRLOG_Spacer + STRLOG_ErrorMessage + errorMessage;
                }

                Logfile.WriteCompleted(STRLOG_ClassName, STRLOG_MethodName, logMessage);

                return executeCommandInfo;

        }

        //-------------------------------------------------------------------------------------------------//

        //public override ExecuteCommandInfo ProcessCommand(ExecuteCommandInfo executeCommandInfo)
        //{
        //    const string STRLOG_MethodName = "ProcessCommand";

        //    Logfile.WriteCalled(STRLOG_ClassName, STRLOG_MethodName);

        //    CommandInfo commandInfo = (CommandInfo)executeCommandInfo;

        //    bool success = true;
        //    string errorMessage = null;

        //    try
        //    {
                
        //         Process the execute command
                
        //        ExecuteCommands executeCommand = (ExecuteCommands)commandInfo.command;

        //        switch (executeCommand)
        //        {
        //            case ExecuteCommands.SetTubeDistance:

                        
        //                 Set tube distance
                        
        //                int tubeDistance = (int)commandInfo.parameters[0];
        //                if ((success = this.flexMotion.SetTubeDistance(tubeDistance)) == false)
        //                {
        //                    errorMessage = this.flexMotion.GetLastError();
        //                }
        //                break;

        //            case ExecuteCommands.SetSourceLocation:

                        
        //                 Set source location
                        
        //                char sourceLocation = (char)commandInfo.parameters[0];
        //                if ((success = this.flexMotion.SetSourceLocation(sourceLocation)) == false)
        //                {
        //                    errorMessage = this.flexMotion.GetLastError();
        //                }
        //                break;

        //            case ExecuteCommands.SetAbsorberLocation:

                        
        //                 Set absorber location
                        
        //                char absorberLocation = (char)commandInfo.parameters[0];
        //                if ((success = this.flexMotion.SetAbsorberLocation(absorberLocation)) == false)
        //                {
        //                    errorMessage = this.flexMotion.GetLastError();
        //                }
        //                break;

        //            case ExecuteCommands.GetCaptureData:

                        
        //                 Get duration from parameters
                        
        //                int duration = (int)commandInfo.parameters[0];

                        
        //                 Get radiation count
                        
        //                int count = -1;
        //                if (this.radiationCounterType == RadiationCounterTypes.ST360)
        //                {
        //                    int[] counts = new int[1];
        //                    if (this.st360Counter.CaptureData(duration, counts) == true)
        //                    {
        //                        count = counts[0];
        //                    }
        //                }
        //                else if (this.radiationCounterType == RadiationCounterTypes.Physics)
        //                {
        //                    count = this.physicsCounter.CaptureData(duration);
        //                }

                        
        //                 Add radiation count to results
                        
        //                commandInfo.results = new object[] { count };
        //                break;

        //            case ExecuteCommands.WriteLcdLine:

                        
        //                 Get LCD line number from request
                        
        //                int lcdLineNo = (int)commandInfo.parameters[0];

                        
        //                 Get LCD message from request and 'URL Decode' to preserve spaces
                        
        //                string lcdMessage = (string)commandInfo.parameters[1];
        //                lcdMessage = HttpUtility.UrlDecode(lcdMessage);

                        
        //                 Write message to LCD
                        
        //                if ((success = this.serialLcd.WriteLine(lcdLineNo, lcdMessage)) == false)
        //                {
        //                    errorMessage = this.serialLcd.GetLastError();
        //                }
        //                break;

        //            default:

                        
        //                 Unknown command
                        
        //                errorMessage = STRERR_UnknownCommand + executeCommand.ToString();
        //                success = false;
        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        success = false;
        //        errorMessage = ex.Message;
        //    }

            
        //     Update success of command execution
            
        //    executeCommandInfo.success = success;

        //    string logMessage = STRLOG_Success + success.ToString();
        //    if (success == false)
        //    {
        //        executeCommandInfo.errorMessage = errorMessage;
        //        logMessage += Logfile.STRLOG_Spacer + STRLOG_ErrorMessage + errorMessage;
        //    }

        //    Logfile.WriteCompleted(STRLOG_ClassName, STRLOG_MethodName, logMessage);

        //    return executeCommandInfo;
        //}

        //-------------------------------------------------------------------------------------------------//

        //-------------------------------------------------------------------------------------------------//

        //-------------------------------------------------------------------------------------------------//

        //-------------------------------------------------------------------------------------------------//

        //-------------------------------------------------------------------------------------------------//

        //-------------------------------------------------------------------------------------------------//

        //-------------------------------------------------------------------------------------------------//

        //-------------------------------------------------------------------------------------------------//

        //-------------------------------------------------------------------------------------------------//

        //-------------------------------------------------------------------------------------------------//

    }
}
