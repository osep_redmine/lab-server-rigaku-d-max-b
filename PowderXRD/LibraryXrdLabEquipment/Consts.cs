﻿using System;

namespace Library.LabEquipment
{
    public class Consts
    {
        //
        // XML elements in the equipment request strings
        //

        public const string STRXML_ReqCurrentStatus = "currentStatus";
        public const string STRXML_ReqCurrentAngle = "currentAngle";


        //
        // XML elements in the equipment response strings
        //
        public const string STRXML_RspCurrentStatus = "currentStatus";
        public const string STRXML_RspCurrentAngle = "currentAngle";

        //
        // XML elements in the EquipmentConfig.xml file
        //
        public const string STRXML_hardwarePresent = "hardwarePresent";
        public const string STRXML_xrdController = "xrdController";
        public const string STRXML_baseline = "baseline";
        public const string STRXML_windows = "windows";
        public const string STRXML_lrSpecification = "lrSpecification";
        public const string STRXML_hvSpecification = "hvSpecification";
        public const string STRXML_voltage = "voltage";
        public const string STRXML_unitType = "unitType";
        public const string STRXML_motorSpeed = "motorSpeed";
        public const string STRXML_commType = "commType";
        public const string STRXML_commParams = "commParams";
        public const string STRXML_type = "type";
        public const string STRXML_network = "network";
        public const string STRXML_ipaddr = "ipaddr";
        public const string STRXML_port = "port";
        public const string STRXML_serial = "serial";
        public const string STRXML_baud = "baud";
        public const string STRXML_writeTimeOut = "writeTimeOut";
        public const string STRXML_readTimeOut = "writeTimeOut";
        public const string STRXML_dmaxRigakuB = "Rigaku D-Max/B";


        // XML elements 
        public const string STRXML_ReqStartAngle = "startAngle";
        public const string STRXML_ReqEndAngle = "endAngle";
        public const string STRXML_ReqStepWidth = "stepWidth";
        public const string STRXML_ReqSamplingTime = "samplingTime";
        public const string STRXML_ReqAxis = "axis";
        public const string STRXML_ReqAngle = "angle";

        public const string STRXML_DataVector = "dataVector";

    }
}
