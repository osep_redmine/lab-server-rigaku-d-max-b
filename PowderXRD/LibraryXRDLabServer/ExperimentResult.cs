﻿using System;
using System.Xml;
using Library.Lab;
using Library.LabServerEngine;

namespace Library.XrdLabServer
{
    public class ExperimentResult : LabExperimentResult
    {
        #region Class Constants and Variables

        private const string STRLOG_ClassName = "ExperimentResult";

        #endregion

        //-------------------------------------------------------------------------------------------------//

        public ExperimentResult(Configuration configuration)
            : base(configuration)
        {
            try
            {
                //
                // Check that all required XML nodes exist
                //
                XmlUtilities.GetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_equipment, true);
                XmlUtilities.GetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_scanType, true);
                XmlUtilities.GetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_axis, true);
                XmlUtilities.GetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_startAngle, true);
                XmlUtilities.GetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_endAngle, true);
                XmlUtilities.GetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_stepWidth, true);
                XmlUtilities.GetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_samplingTime, true);
                XmlUtilities.GetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_dataType, true);
                XmlUtilities.GetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_dataVector, true);
            }
            catch (Exception ex)
            {
                // Log the message and throw the exception back to the caller
                Logfile.WriteError(ex.Message);
                throw;
            }
        }

        //-------------------------------------------------------------------------------------------------//

        public ExperimentResult(int experimentId, string sbName, DateTime dateTime, int unitId, Configuration configuration,
            Specification specification, ResultInfo resultInfo)
            : base(experimentId, sbName, dateTime, specification.SetupId, unitId, configuration)
        {
            const string STRLOG_MethodName = "ExperimentResult";

            Logfile.WriteCalled(null, STRLOG_MethodName);

            try
            {
                //
                // Add the specification information
                //
                XmlUtilities.SetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_equipment, specification.Equipment, false);
                XmlUtilities.SetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_scanType, specification.ScanType, false);
                XmlUtilities.SetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_axis, specification.Axis, false);
                XmlUtilities.SetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_startAngle, specification.StartAngle.ToString(), false);
                XmlUtilities.SetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_endAngle, specification.EndAngle.ToString(), false);
                XmlUtilities.SetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_stepWidth, specification.StepWidth.ToString(), false);
                XmlUtilities.SetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_samplingTime, specification.SamplingTime.ToString(), false);

                if (resultInfo.statusCode == StatusCodes.Completed)
                {
                    if (specification.SetupId.Equals(Consts.STRXML_setupId_IntensityVsAngle))
                    {                        
                        // Powder XRD is single run lab for the time being.
                        XmlUtilities.SetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_dataType, resultInfo.dataType.ToString(), false);
                        XmlUtilities.SetXmlValue(this.xmlNodeExperimentResult, Consts.STRXML_dataVector, resultInfo.dataVector, true);                       
                    }
                }
            }
            catch (Exception ex)
            {
                Logfile.WriteError(ex.Message);
                throw;
            }

            Logfile.WriteCompleted(null, STRLOG_MethodName);
        }
    }
}
