﻿using System;
using System.Xml;
using Library.Lab;
using Library.LabServerEngine;

namespace Library.XrdLabServer
{
    public class Configuration : LabConfiguration
    {
        #region Class Constants and Variables

        private const string STRLOG_ClassName = "Configuration";

        //
        // String constants for the XML lab configuration
        //
        private const string STRXML_equipment = "equipment";
        private const string STRXML_equipments = "equipments";
        private const string STRXML_axis = "axis";
        private const string STRXML_axes = "axes";
        private const string STRXML_scanType = "scanType";
        private const string STRXML_scanTypes = "scanTypes";
        private const string STRXMLPARAM_default = "@default";
        private const string STRXML_name = "name";
        private const string STRXML_description = "description";
        private const string STRXML_startAngle = "startAngle";
        private const string STRXML_endAngle = "endAngle";
        private const string STRXML_stepWidth = "stepWidth";
        private const string STRXML_samplingTime = "samplingTime";
        private const string STRXML_minimum = "minimum";
        private const string STRXML_maximum = "maximum";
        private const string STRXML_unitSize = "unitSize";


        //
        // String constants for logfile messages
        //
        private const string STRLOG_Equipment = " Equipment: ";
        private const string STRLOG_Axis = " Axis: ";
        private const string STRLOG_ScanType = " Scan Type: ";

        #endregion

        #region Properties

        private string[] equipmentNames;
        private string[] axisNames;
        private string[] scanTypeNames;

        public string[] EquipmentNames
        {
            get { return this.equipmentNames; }
        }

        public string[] AxisNames
        {
            get { return this.axisNames; }
        }

        public string[] ScanTypeNames
        {
            get { return this.scanTypeNames; }
        }

        #endregion

        //-------------------------------------------------------------------------------------------------//

        public Configuration(string rootFilePath)
            : this(rootFilePath, null)
        {
        }

        //---------------------------------------------------------------------------------------//

        public Configuration(string rootFilePath, string xmlLabConfiguration)
            : base(rootFilePath, xmlLabConfiguration)
        {
            const string STRLOG_MethodName = "Configuration";

            Logfile.WriteCalled(null, STRLOG_MethodName);

            try
            {
                //
                // Get a list of all Equipments, must have at least one
                //
                XmlNode xmlNodeEquipments = XmlUtilities.GetXmlNode(this.xmlNodeConfiguration, STRXML_equipments);
                XmlNodeList xmlNodeList = XmlUtilities.GetXmlNodeList(xmlNodeEquipments, STRXML_equipment, false);
                if (xmlNodeList.Count > 0)
                {
                    this.equipmentNames = new string[xmlNodeList.Count];

                    for (int i = 0; i < xmlNodeList.Count; i++)
                    {
                        XmlNode xmlNodeTemp = xmlNodeList.Item(i);
                        this.equipmentNames[i] = XmlUtilities.GetXmlValue(xmlNodeTemp, STRXML_name, false);
                        Logfile.Write(STRLOG_Equipment + this.equipmentNames[i]);
                    }
                }

                //
                // Get a list of all Scan Types, must have at least one
                //
                XmlNode xmlNodeScanTypes = XmlUtilities.GetXmlNode(this.xmlNodeConfiguration, STRXML_scanTypes);
                xmlNodeList = XmlUtilities.GetXmlNodeList(xmlNodeScanTypes, STRXML_scanType, false);
                if (xmlNodeList.Count > 0)
                {
                    this.scanTypeNames = new string[xmlNodeList.Count];

                    for (int i = 0; i < xmlNodeList.Count; i++)
                    {
                        XmlNode xmlNodeTemp = xmlNodeList.Item(i);
                        this.ScanTypeNames[i] = XmlUtilities.GetXmlValue(xmlNodeTemp, STRXML_name, false);
                        Logfile.Write(STRLOG_ScanType + this.ScanTypeNames[i]);
                    }
                }

                //
                // Get a list of all Scan Types, must have at least one
                //
                XmlNode xmlNodeaxes = XmlUtilities.GetXmlNode(this.xmlNodeConfiguration, STRXML_axes);
                xmlNodeList = XmlUtilities.GetXmlNodeList(xmlNodeaxes, STRXML_axis, false);
                if (xmlNodeList.Count > 0)
                {
                    this.axisNames = new string[xmlNodeList.Count];

                    for (int i = 0; i < xmlNodeList.Count; i++)
                    {
                        XmlNode xmlNodeTemp = xmlNodeList.Item(i);
                        this.axisNames[i] = XmlUtilities.GetXmlValue(xmlNodeTemp, STRXML_name, false);
                        Logfile.Write(STRLOG_Axis + this.axisNames[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                Logfile.WriteError(ex.Message);
                throw;
            }

            Logfile.WriteCompleted(null, STRLOG_MethodName);
        }

    }
}
