﻿using System;
using System.Diagnostics;
using System.Xml;
using Library.Lab;
using Library.LabServerEngine;
using Library.LabServerEngine.Drivers.Setup;
using Library.LabServerEngine.Drivers.Equipment;

namespace Library.XrdLabServer.Drivers.Setup
{
    public partial class DriverPowderXrd : DriverEquipmentGeneric
    {
        public override int GetExecutionTime(ExperimentSpecification experimentSpecification)
        {
            const string STRLOG_MethodName = "GetExecutionTime";

            Logfile.WriteCalled(STRLOG_ClassName, STRLOG_MethodName);

            Specification specification = (Specification)experimentSpecification;

            double executionTime = -1;

            try
            {
                //
                // First, check to see if the LabEquipment is online
                //
                LabEquipmentStatus labEquipmentStatus = this.equipmentServiceProxy.GetLabEquipmentStatus();
                if (labEquipmentStatus.online == false)
                {
                    throw new Exception(labEquipmentStatus.statusMessage);
                }

                //
                // Get the time until the LabEquipment is ready to use
                //
                executionTime = this.equipmentServiceProxy.GetTimeUntilReady();

                // This should be done in the equipment side.
                if (specification.ScanType == Consts.STRXML_scanType_step)
                {
                    // Calculate Scan Time

                    double angleRange = (specification.EndAngle - specification.StartAngle);
                    double scanTime = (angleRange / specification.StepWidth) * specification.SamplingTime;

                    // Calculate Motor Time
                    double motorTime = angleRange * Specification.MotorTime;
                    waitTime = (int)(scanTime + motorTime+ 0.5);
                    // executionTime += 2 * ( waitTime);
                    executionTime = waitTime + 120; // add two minitues as a buffer.
                }
                else
                {
                    Logfile.Write("Invalid scan type passed");
                }
            }
            catch (Exception ex)
            {
                Logfile.WriteError(ex.Message);
                throw;
            }

            //
            // Round execution time to the nearest integer
            //
            int execTime = (int)(executionTime + 0.5);

            string logMessage = STRLOG_ExecutionTime + execTime.ToString();

            Logfile.WriteCompleted(STRLOG_ClassName, STRLOG_MethodName, logMessage);

            return execTime;

        }
    }
}
