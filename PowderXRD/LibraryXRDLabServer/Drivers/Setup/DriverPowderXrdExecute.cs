﻿using System;
using System.Diagnostics;
using System.Xml;
using System.Web;
using Library.Lab;
using Library.LabServerEngine;
using Library.LabServerEngine.Drivers.Setup;
using Library.LabServerEngine.Drivers.Equipment;

namespace Library.XrdLabServer.Drivers.Setup
{
    public partial class DriverPowderXrd : DriverEquipmentGeneric
    {
        #region Class Constants and Variables


        private enum States_Execute
        {
            sSuspendPowerdown,
            sSimpleScan,
            sGetDataVectorReadyStatus,
            sGetDataVector,
            sResumePowerdown,
            sCompleted,
            sOpenSerialPort,
            sOpenXrayShutter,
            sCloseSerialPort,
            sCloseXrayShutter,
        }

        private struct SMTableEntry_Execute
        {
            public States_Execute currentState;
            public States_Execute nextState;
            public States_Execute exitState;
            public string equipmentCommand;
            public string[,] commandArguments;

            public SMTableEntry_Execute(States_Execute currentState, States_Execute nextState, States_Execute exitState,
                string equipmentCommand, string[,] commandArguments)
            {
                this.currentState = currentState;
                this.nextState = nextState;
                this.exitState = exitState;
                this.equipmentCommand = equipmentCommand;
                this.commandArguments = commandArguments;
            }
        }

        private SMTableEntry_Execute[] smTable_Execute = new SMTableEntry_Execute[] {
                        //
            // Suspend powerdown
            //
            new SMTableEntry_Execute(States_Execute.sSuspendPowerdown, States_Execute.sOpenSerialPort, States_Execute.sResumePowerdown,
                null, null),

            new SMTableEntry_Execute(States_Execute.sOpenSerialPort, States_Execute.sOpenXrayShutter, States_Execute.sCloseSerialPort, 
                Consts.STRXML_CmdOpenSerialPort, null ),

            new SMTableEntry_Execute(States_Execute.sOpenXrayShutter, States_Execute.sSimpleScan, States_Execute.sCloseXrayShutter,
                Consts.STRXML_CmdOpenXrayShutter, null ),

            new SMTableEntry_Execute(States_Execute.sSimpleScan, States_Execute.sGetDataVector, States_Execute.sCloseXrayShutter, Consts.STRXML_CmdSimpleScan, new string[,]{
                { Consts.STRXML_startAngle, string.Empty },
                { Consts.STRXML_endAngle, string.Empty },
                { Consts.STRXML_stepWidth, string.Empty },
                { Consts.STRXML_samplingTime, string.Empty },
            } ),

           new SMTableEntry_Execute(States_Execute.sGetDataVector, States_Execute.sCloseXrayShutter, States_Execute.sCloseXrayShutter, Consts.STRXML_CmdGetDataVector, null ),

           new SMTableEntry_Execute(States_Execute.sCloseXrayShutter, States_Execute.sCloseSerialPort, States_Execute.sCloseSerialPort, 
                Consts.STRXML_CmdCloseXrayShutter, null ),

            new SMTableEntry_Execute(States_Execute.sCloseSerialPort, States_Execute.sResumePowerdown, States_Execute.sResumePowerdown,
                Consts.STRXML_CmdCloseSerialPort, null ),

            //
            // Resume powerdown
            //
            new SMTableEntry_Execute(States_Execute.sResumePowerdown, States_Execute.sCompleted, States_Execute.sCompleted,
                null, null),

        };
        #endregion

        public override ExperimentResultInfo Execute(ExperimentSpecification experimentSpecification)
        {
            const string STRLOG_MethodName = "Execute";

            Logfile.WriteCalled(STRLOG_ClassName, STRLOG_MethodName);

            //
            // Determine how long it actually take to execute
            //
            DateTime startDateTime = DateTime.Now;

            // Typecast the specification so that it can be used here
            Specification specification = (Specification)experimentSpecification;

            //
            // Log the specification
            //
            string logMessage = string.Empty;
            //
            // YOUR CODE HERE
            //
            int totalWaitTime = -1;
            bool waitForDataVector = false;

            Logfile.Write(logMessage);

            //
            // Create an instance of the result info ready to fill in
            //
            ResultInfo resultInfo = new ResultInfo();
            resultInfo.statusCode = StatusCodes.Running;
            resultInfo.dataType = DataTypes.Real;

            //
            // Create data structures to hold the results
            //
            int vectorLength = (int)(((specification.EndAngle - specification.StartAngle) / specification.StepWidth) + 1);
            resultInfo.dataVector = string.Empty;

            try
            {
                //
                // First, check to see if the LabEquipment is online
                //
                LabEquipmentStatus labEquipmentStatus = this.equipmentServiceProxy.GetLabEquipmentStatus();
                if (labEquipmentStatus.online == false)
                {
                    throw new Exception(labEquipmentStatus.statusMessage);
                }

                //
                // Run the state machine to execute the experiment specification
                //
                States_Execute state = States_Execute.sCompleted;
                if (smTable_Execute.Length > 0)
                {
                    state = smTable_Execute[0].currentState;
                }
                while (state != States_Execute.sCompleted)
                {

                    // Logfile.Write("---------------------- STATE : " + state.ToString().ToUpper());

                    //
                    // Find table entry
                    //
                    int index = -1;
                    for (int i = 0; i < smTable_Execute.Length; i++)
                    {
                        if (smTable_Execute[i].currentState == state)
                        {
                            // Entry found
                            index = i;
                            break;
                        }
                    }
                    if (index == -1)
                    {
                        throw new ArgumentOutOfRangeException(state.ToString(), STRERR_StateNotFound);
                    }

                    //
                    // Get table entry and save next state
                    //
                    SMTableEntry_Execute entry = smTable_Execute[index];
                    States_Execute nextState = entry.nextState;

                    logMessage = " [ " + STRLOG_MethodName + ": " + entry.currentState.ToString() + " ]";
                    Logfile.Write(logMessage);

                    Trace.WriteLine(logMessage);

                    //
                    // Check if experiment was cancelled
                    //
                    if (this.cancelExperiment != null && this.cancelExperiment.IsCancelled == true &&
                        resultInfo.statusCode == StatusCodes.Running)
                    {
                        //
                        // Experiment was cancelled
                        //
                        resultInfo.statusCode = StatusCodes.Cancelled;
                        state = entry.exitState;
                        continue;
                    }

                    //
                    // Process non-XML commands
                    //
                    switch (entry.currentState)
                    {
                        case States_Execute.sSuspendPowerdown:
                            if (this.equipmentServiceProxy.SuspendPowerdown() == false)
                            {
                                //
                                // Command execution failed
                                //
                                resultInfo.statusCode = StatusCodes.Failed;
                                resultInfo.errorMessage = STRERR_SuspendPowerdown;
                                state = entry.exitState;
                            }
                            else
                            {
                                state = nextState;
                            }
                            continue;

                        case States_Execute.sResumePowerdown:
                            if (this.equipmentServiceProxy.ResumePowerdown() == false)
                            {
                                //
                                // Command execution failed
                                //
                                resultInfo.statusCode = StatusCodes.Failed;
                                resultInfo.errorMessage = STRERR_ResumePowerdown;
                                state = entry.exitState;
                            }
                            else
                            {
                                state = nextState;
                            }
                            continue;

                        default:
                            break;
                    }

                    //
                    // Add command arguments where required
                    //
                    switch (entry.currentState)
                    {
                        case States_Execute.sSimpleScan:
                            entry.commandArguments[0, 1] = specification.StartAngle.ToString();
                            entry.commandArguments[1, 1] = specification.EndAngle.ToString();
                            entry.commandArguments[2, 1] = specification.StepWidth.ToString();
                            entry.commandArguments[3, 1] = specification.SamplingTime.ToString();
                            break;

                        default:
                            break;
                    }

                    //
                    // Execute command and check response success
                    //
                    DateTime start = DateTime.Now;
                    XmlDocument xmlRequestDocument = CreateXmlRequestDocument(entry.equipmentCommand, entry.commandArguments);

                    string xmlResponse = this.equipmentServiceProxy.ExecuteRequest(xmlRequestDocument.InnerXml);

                    Logfile.Write("Xml Response :" + "\n" + xmlResponse);

                    XmlNode xmlResponseNode = CreateXmlResponseNode(xmlResponse);
                    if (XmlUtilities.GetBoolValue(xmlResponseNode, LabServerEngine.Consts.STRXML_RspSuccess, false) == false)
                    {
                        if( entry.currentState == States_Execute.sGetDataVector)
                        {
                            if (waitForDataVector == false)
                            {
                                waitForDataVector = true;
                                //totalWaitTime = (int)((this.GetExecutionTime(specification) * 0.75 )+ 60 + 0.5 );
                                totalWaitTime = (int)this.GetExecutionTime(specification);
                            }


                            if (totalWaitTime > 0)
                            {
                                Logfile.Write("Total Wait Time : " + totalWaitTime.ToString() + " : " + DateTime.Now.ToString());
                                totalWaitTime--;
                                System.Threading.Thread.Sleep(1000);       // get this timing right !    
                                continue;
                            }
                        }

                            //
                            // Command execution failed
                            //
                            resultInfo.statusCode = StatusCodes.Failed;
                            resultInfo.errorMessage = XmlUtilities.GetXmlValue(xmlResponseNode, LabServerEngine.Consts.STRXML_RspErrorMessage, true);
                            state = entry.exitState;

                        continue;
                    }

                    // extract responses where it is needed.
                    switch (entry.currentState)
                    {
                        case States_Execute.sGetDataVector:
                            resultInfo.dataVector = XmlUtilities.GetXmlValue(xmlResponseNode, Consts.STRXML_dataVector, true);
                            break;
                        default:
                            break;
                    }


                    TimeSpan commandExecutionTime = DateTime.Now - start;
                    Trace.WriteLine("Command: " + xmlRequestDocument.InnerXml);
                    Trace.WriteLine("Execution Time: " + commandExecutionTime.TotalSeconds.ToString());

                    //
                    // Next state
                    //
                    state = nextState;
                }

                //
                // Update status code
                //
                if (resultInfo.statusCode == StatusCodes.Running)
                {
                    resultInfo.statusCode = StatusCodes.Completed;
                }
            }
            catch (Exception ex)
            {
                resultInfo.statusCode = StatusCodes.Failed;
                resultInfo.errorMessage = ex.Message + ":in XRD";
                Logfile.WriteError(ex.Message);
            }

            //
            // Calculate actual execution time and round to the nearest integer
            //
            TimeSpan timeSpan = DateTime.Now - startDateTime;
            int execTime = (int)(timeSpan.TotalSeconds + 0.5);

            logMessage = STRLOG_StatusCode + resultInfo.statusCode
                + Logfile.STRLOG_Spacer + STRLOG_ExecutionTime + execTime.ToString();

            Logfile.WriteCompleted(STRLOG_ClassName, STRLOG_MethodName, logMessage);

            return resultInfo;
        }
    }
}

