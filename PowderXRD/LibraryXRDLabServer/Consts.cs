﻿using System;

namespace Library.XrdLabServer
{
    public class Consts
    {
        //
        // Application configuration file key strings
        //
        public const string STRCFG_XmlSimulationConfigFilename = "XmlSimulationConfigFilename";

        //
        // XML Configuration
        //
        public const string STRXML_setupId_IntensityVsAngle = "IntensityVsAngle";

        //
        // XML Specification and ExperimentResult
        //
        public const string STRXML_equipment = "equipment";
        public const string STRXML_scanType = "scanType";
        public const string STRXML_axis = "axis";
        public const string STRXML_startAngle = "startAngle";
        public const string STRXML_endAngle = "endAngle";
        public const string STRXML_stepWidth = "stepWidth";
        public const string STRXML_samplingTime = "samplingTime";
        public const string STRXML_waitTime = "waitTime";
        public const string CHR_CsvSplitter = ",";
        //
        // XML Validation
        //
        public const string STRXML_vdnStartAngle = "vdnStartAngle";
        public const string STRXML_vdnEndAngle = "vdnEndAngle";
        public const string STRXML_vdnStepWidth = "vdnStepWidth";
        public const string STRXML_vdnSamplingTime = "vdnSamplingTime";
        public const string STRXML_vdnTotaltime = "vdnTotalTime";
        public const string STRXML_minimum = "minimum";
        public const string STRXML_maximum = "maximum";
        public const string STRXML_unitSize = "unitSize";

 
        //
        // XML ExperimentResult
        //
        public const string STRXML_dataType = "dataType";
        public const string STRXML_dataVector = "dataVector";

        //
        // ScanTypes
        //
        public const string STRXML_scanType_step = "step";
        public const string STRXML_scanType_continous = "continous";
 
        //
        // Commands
        public const string STRXML_CmdSimpleScan = "SimpleScan";
        public const string STRXML_CmdOpenXrayShutter = "SetXrayShutterOpen";
        public const string STRXML_CmdCloseXrayShutter = "SetXrayShutterClose";
        public const string STRXML_CmdOpenSerialPort = "SetSerialPortOpen";
        public const string STRXML_CmdCloseSerialPort = "SetSerialPortClose";
        public const string STRXML_CmdGetDataVectorReadyStatus = "GetDataVectorReadyStatus";
        public const string STRXML_CmdGetDataVector = "GetDataVector";

    }
}
