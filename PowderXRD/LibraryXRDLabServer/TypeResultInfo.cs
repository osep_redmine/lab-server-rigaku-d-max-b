﻿using System;
using Library.LabServerEngine;

namespace Library.XrdLabServer
{
    public enum DataTypes
    {
        Unknown, Real, Simulated, Calculated
    };

    public class ResultInfo : ExperimentResultInfo
    {
        public DataTypes dataType;
        public string dataVector;

        //-------------------------------------------------------------------------------------------------//

        public ResultInfo()
            : base()
        {
            this.dataType = DataTypes.Unknown;
        }
    }

}
