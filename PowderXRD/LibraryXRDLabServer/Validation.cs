﻿using System;
using System.Xml;
using Library.Lab;
using Library.LabServerEngine;
using Library.XrdLabServer.Drivers;

namespace Library.XrdLabServer
{
    public class Validation : ExperimentValidation
    {
        #region Class Constants and Variables

        private const string STRLOG_ClassName = "Validation";

        //
        // String constants for error messages
        //
        private const string STRERR_StartAngle = " Start Angle (";
        private const string STRERR_EndAngle = " End Angle (";
        private const string STRERR_StepWidth = " Step Width (";
        private const string STRERR_SamplingTime = " Sampling Time (";
        private const string STRERR_TotalTime = " Total time (";
        private const string STRERR_IsLessThan = ") is less than ";
        private const string STRERR_IsGreaterThan = ") is greater than ";
        private const string STRERR_ReduceTotalTime = " - Reduce the range of scan, reduce the sampling time or increase the step width to reduce the total time";

        //
        // Local types
        //
        private struct VdnMinMaxUnitSize
        {
            public double min;
            public double max;
            public double unitSize;
        }

        private struct VdnMinMax{
            public int min;
            public int max;
        }

        //
        // Local variables
        //
        private VdnMinMaxUnitSize startAngle;
        private VdnMinMaxUnitSize endAngle;
        private VdnMinMaxUnitSize stepWidth;
        private VdnMinMaxUnitSize samplingTime;

        private VdnMinMax totalTime;

        #endregion

        //-------------------------------------------------------------------------------------------------//

        public Validation(Configuration configuration)
            : base(configuration)
        {
            const string STRLOG_MethodName = "Validation";

            Logfile.WriteCalled(null, STRLOG_MethodName);

            //
            // Get information from the validation XML node
            //
            try
            {
                //
                // Get Start Angle information from the validation node
                //
                XmlNode xmlNode = XmlUtilities.GetXmlNode(this.xmlNodeValidation, Consts.STRXML_vdnStartAngle);
                this.startAngle = new VdnMinMaxUnitSize();
                this.startAngle.min = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_minimum);
                this.startAngle.max = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_maximum);
                this.startAngle.unitSize = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_unitSize);

                //
                // Get Start Angle information from the validation node
                //
                xmlNode = XmlUtilities.GetXmlNode(this.xmlNodeValidation, Consts.STRXML_vdnEndAngle);
                this.endAngle = new VdnMinMaxUnitSize();
                this.endAngle.min = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_minimum);
                this.endAngle.max = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_maximum);
                this.endAngle.unitSize = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_unitSize);

                //
                // Get Start Angle information from the validation node
                //
                xmlNode = XmlUtilities.GetXmlNode(this.xmlNodeValidation, Consts.STRXML_vdnStepWidth);
                this.stepWidth = new VdnMinMaxUnitSize();
                this.stepWidth.min = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_minimum);
                this.stepWidth.max = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_maximum);
                this.stepWidth.unitSize = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_unitSize);

                //
                // Get Start Angle information from the validation node
                //
                xmlNode = XmlUtilities.GetXmlNode(this.xmlNodeValidation, Consts.STRXML_vdnSamplingTime);
                this.samplingTime = new VdnMinMaxUnitSize();
                this.samplingTime.min = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_minimum);
                this.samplingTime.max = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_maximum);
                this.samplingTime.unitSize = XmlUtilities.GetRealValue(xmlNode, Consts.STRXML_unitSize);

                xmlNode = XmlUtilities.GetXmlNode(this.xmlNodeValidation, Consts.STRXML_vdnTotaltime);
                this.totalTime = new VdnMinMax();
                this.totalTime.min = XmlUtilities.GetIntValue(xmlNode, Consts.STRXML_minimum);
                this.totalTime.max = XmlUtilities.GetIntValue(xmlNode, Consts.STRXML_maximum);

            }
            catch (Exception ex)
            {
                Logfile.WriteError(ex.Message);
                throw;
            }

            Logfile.WriteCompleted(null, STRLOG_MethodName);
        }

        ////-------------------------------------------------------------------------------------------------//

        //public void ValidateDistance(int distance)
        //{
        //    try
        //    {
        //        if (distance < this.distance.min)
        //        {
        //            throw new ArgumentException(STRERR_Distance + distance.ToString() +
        //                STRERR_IsLessThan + this.distance.min.ToString());
        //        }
        //        if (distance > this.distance.max)
        //        {
        //            throw new ArgumentException(STRERR_Distance + distance.ToString() +
        //                STRERR_IsGreaterThan + this.distance.max.ToString());
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        // Throw error back to caller
        //        throw;
        //    }
        //}

        ////-------------------------------------------------------------------------------------------------//

        //public void ValidateDuration(int duration)
        //{
        //    try
        //    {
        //        if (duration < this.duration.min)
        //        {
        //            throw new ArgumentException(STRERR_Duration + duration.ToString() +
        //                STRERR_IsLessThan + this.duration.min.ToString());
        //        }
        //        if (duration > this.duration.max)
        //        {
        //            throw new ArgumentException(STRERR_Duration + duration.ToString() +
        //                STRERR_IsGreaterThan + this.duration.max.ToString());
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        // Throw error back to caller
        //        throw;
        //    }
        //}

        ////-------------------------------------------------------------------------------------------------//

        //public void ValidateRepeat(int repeat)
        //{
        //    try
        //    {
        //        if (repeat < this.repeat.min)
        //        {
        //            throw new ArgumentException(STRERR_Repeat + repeat.ToString() +
        //                STRERR_IsLessThan + this.repeat.min.ToString());
        //        }
        //        if (repeat > this.repeat.max)
        //        {
        //            throw new ArgumentException(STRERR_Repeat + repeat.ToString() +
        //                STRERR_IsGreaterThan + this.repeat.max.ToString());
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        // Throw error back to caller
        //        throw;
        //    }
        //}

        ////-------------------------------------------------------------------------------------------------//

        //-------------------------------------------------------------------------------------------------//
        public void ValidateStartAngle(double startAngle)
        {
            try
            {
                if (startAngle < this.startAngle.min)
                {
                    throw new ArgumentException(STRERR_StartAngle + startAngle.ToString() +
                        STRERR_IsLessThan + this.totalTime.min.ToString());
                }

                if (startAngle > this.startAngle.max)
                {
                    throw new ArgumentException(STRERR_StartAngle + startAngle.ToString() +
                        STRERR_IsGreaterThan + this.totalTime.min.ToString());
                }

                // TODO: Implement unitSize error.
            }
            catch
            {
                //Throw error back to caller
                throw;
            }
        }

        //-------------------------------------------------------------------------------------------------//
        public void ValidateEndAngle(double endAngle)
        {
            try
            {
                if (endAngle < this.endAngle.min)
                {
                    throw new ArgumentException(STRERR_EndAngle + endAngle.ToString() +
                        STRERR_IsLessThan + this.totalTime.min.ToString());
                }

                if (endAngle > this.endAngle.max)
                {
                    throw new ArgumentException(STRERR_EndAngle + endAngle.ToString() +
                        STRERR_IsGreaterThan + this.totalTime.min.ToString());
                }

                // TODO: Implement unitSize error.
            }
            catch
            {
                //Throw error back to caller
                throw;
            }
        }

        //-------------------------------------------------------------------------------------------------//
        public void ValidateStepWidth(double stepWidth)
        {
            try
            {
                if (stepWidth < this.stepWidth.min)
                {
                    throw new ArgumentException(STRERR_StepWidth + stepWidth.ToString() +
                        STRERR_IsLessThan + this.totalTime.min.ToString());
                }

                if (stepWidth > this.stepWidth.max)
                {
                    throw new ArgumentException(STRERR_StepWidth + stepWidth.ToString() +
                        STRERR_IsGreaterThan + this.totalTime.min.ToString());
                }

                // TODO: Implement unitSize error.
            }
            catch
            {
                //Throw error back to caller
                throw;
            }
        }

        //-------------------------------------------------------------------------------------------------//
        public void ValidateSamplingTime(double samplingTime)
        {
            try
            {
                if (samplingTime < this.samplingTime.min)
                {
                    throw new ArgumentException(STRERR_SamplingTime + samplingTime.ToString() +
                        STRERR_IsLessThan + this.totalTime.min.ToString());
                }

                if (samplingTime > this.samplingTime.max)
                {
                    throw new ArgumentException(STRERR_SamplingTime + samplingTime.ToString() +
                        STRERR_IsGreaterThan + this.totalTime.min.ToString());
                }

                // TODO: Implement unitSize error.
            }
            catch
            {
                //Throw error back to caller
                throw;
            }
        }

        //-------------------------------------------------------------------------------------------------//

        public void ValidateTotalTime(int totalTime)
        {
            try
            {
                if (totalTime < this.totalTime.min)
                {
                    throw new ArgumentException(STRERR_TotalTime + totalTime.ToString() +
                        STRERR_IsLessThan + this.totalTime.min.ToString());
                }
                if (totalTime > this.totalTime.max)
                {
                    throw new ArgumentException(STRERR_TotalTime + totalTime.ToString() +
                        STRERR_IsGreaterThan + this.totalTime.max.ToString() + STRERR_ReduceTotalTime);
                }
            }
            catch (Exception)
            {
                // Throw error back to caller
                throw;
            }
        }

    }
}
