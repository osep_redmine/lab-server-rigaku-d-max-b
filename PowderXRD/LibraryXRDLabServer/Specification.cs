using System;
using Library.Lab;
using Library.LabServerEngine;
using Library.LabServerEngine.Drivers.Equipment;
using Library.LabServerEngine.Drivers.Setup;
using Library.XrdLabServer.Drivers.Setup;

namespace Library.XrdLabServer
{
    public class Specification : ExperimentSpecification
    {
        #region Class Constants and Variables

        private const string STRLOG_ClassName = "Specification";

        //
        // Constants
        //
        public const double MotorTime= 0.325; // this is a rought estimate.
 
        //
        // String constants for logfile messages
        //

        //
        // String constants for error messages
        //
        private const string STRERR_InvalidEquipment = "Invalid Equipment";
        private const string STRERR_InvalidScanType = "Invalid Scan Type";
        private const string STRERR_InvalidAxis = "Invalid Axis";

        //
        // Local variables
        //
        private Configuration configuration;
        private Validation validation;

        #endregion

        #region Properties

        private string equipment;
        private string axis;
        private string scanType;
        private double startAngle;
        private double endAngle;
        private double stepWidth;
        private double samplingTime;

        public string Equipment
        {
            get { return equipment; }
        }

        public string Axis
        {
            get { return axis; }
        }

        public string ScanType
        {
            get { return scanType; }
        }

        public double StartAngle
        {
            get { return startAngle; }
        }

        public double EndAngle
        {
            get { return endAngle; }
        }

        public double StepWidth
        {
            get { return stepWidth; }
        }

        public double SamplingTime
        {
            get { return samplingTime; }
        }

        #endregion

        //-------------------------------------------------------------------------------------------------//

        public Specification(Configuration configuration, EquipmentService equipmentServiceProxy)
            : base(configuration, equipmentServiceProxy)
        {
            const string STRLOG_MethodName = "Specification";

            Logfile.WriteCalled(null, STRLOG_MethodName);

            //
            // Save these for use by the Parse() method
            //
            this.configuration = configuration;

            //
            // Check that the specification template is valid. This is used by the LabClient to submit
            // the experiment specification to the LabServer for execution.
            //
            try
            {
                //
                // Check that all required XML nodes exist
                //
                XmlUtilities.GetXmlValue(this.xmlNodeSpecification, Consts.STRXML_equipment, true);
                XmlUtilities.GetXmlValue(this.xmlNodeSpecification, Consts.STRXML_scanType, true);
                XmlUtilities.GetXmlValue(this.xmlNodeSpecification, Consts.STRXML_axis, true);
                XmlUtilities.GetXmlValue(this.xmlNodeSpecification, Consts.STRXML_startAngle, true);
                XmlUtilities.GetXmlValue(this.xmlNodeSpecification, Consts.STRXML_endAngle, true);
                XmlUtilities.GetXmlValue(this.xmlNodeSpecification, Consts.STRXML_stepWidth, true);
                XmlUtilities.GetXmlValue(this.xmlNodeSpecification, Consts.STRXML_samplingTime, true);

                //
                // Create an instance fo the Validation class
                //
                this.validation = new Validation(configuration);
            }
            catch (Exception ex)
            {
                Logfile.WriteError(ex.Message);
                throw;
            }

            Logfile.WriteCompleted(null, STRLOG_MethodName);
        }

        //-------------------------------------------------------------------------------------------------//

        /// <summary>
        /// Parse the XML specification string to check its validity. No exceptions are thrown back to the
        /// calling method. If an error occurs, 'accepted' is set to false and the error message is placed
        /// in 'errorMessage' where it can be examined by the calling method.
        /// </summary>
        /// <param name="xmlSpecification"></param>
        public override ValidationReport Parse(string xmlSpecification)
        {
            const string STRLOG_MethodName = "Parse";

            Logfile.WriteCalled(STRLOG_ClassName, STRLOG_MethodName);

            //
            // Catch all exceptions and log errors, don't throw back to caller
            //
            ValidationReport validationReport = null;
            try
            {
                //
                // Call the base class to parse its part
                //
                validationReport = base.Parse(xmlSpecification);
                if (validationReport.accepted == false)
                {
                    throw new Exception(validationReport.errorMessage);
                }

                // Create new validation report
                validationReport = new ValidationReport();

                //
                // Validate the specification
                //

                //
                // Get the equipment name and check whether it is valid.
                //
                string strEquipmentName = XmlUtilities.GetXmlValue(this.xmlNodeSpecification, Consts.STRXML_equipment, false);
                int index = Array.IndexOf(this.configuration.EquipmentNames, strEquipmentName);
                if (index < 0)
                {
                    throw new ArgumentException(STRERR_InvalidEquipment, strEquipmentName);
                }
                this.equipment = this.configuration.EquipmentNames[index];

                //
                // Get the axis name and check whether it is valid.
                //
                string strAxisName = XmlUtilities.GetXmlValue(this.xmlNodeSpecification, Consts.STRXML_axis, false);
                index = Array.IndexOf(this.configuration.AxisNames, strAxisName);
                if (index < 0)
                {
                    throw new ArgumentException(STRERR_InvalidAxis, strAxisName);
                }
                this.axis = this.configuration.AxisNames[index];

                //
                // Get the scanType name and check whether it is valid.
                //
                string strScanTypeName = XmlUtilities.GetXmlValue(this.xmlNodeSpecification, Consts.STRXML_scanType, false);
                index = Array.IndexOf(this.configuration.ScanTypeNames, strScanTypeName);
                if (index < 0)
                {
                    throw new ArgumentException(STRERR_InvalidScanType, strScanTypeName);
                }
                this.scanType = this.configuration.ScanTypeNames[index];

                //
                // Get startAngle and validate
                //
                this.startAngle = XmlUtilities.GetRealValue(this.xmlNodeSpecification, Consts.STRXML_startAngle);
                this.validation.ValidateStartAngle(this.startAngle);

                //
                // Get endAngle and validate
                //
                this.endAngle = XmlUtilities.GetRealValue(this.xmlNodeSpecification, Consts.STRXML_endAngle);
                this.validation.ValidateEndAngle(this.endAngle);

                //
                // Get stepWidth and validate
                //
                this.stepWidth = XmlUtilities.GetRealValue(this.xmlNodeSpecification, Consts.STRXML_stepWidth);
                this.validation.ValidateStepWidth(this.stepWidth);

                //
                // Get samplingTime and validate
                //
                this.samplingTime = XmlUtilities.GetRealValue(this.xmlNodeSpecification, Consts.STRXML_samplingTime);
                this.validation.ValidateSamplingTime(this.samplingTime);

                //
                // Create an instance of the driver for the specified setup and then
                // get the driver's execution time for this specification
                //
                int executionTime = -1;
                if (this.SetupId.Equals(Consts.STRXML_setupId_IntensityVsAngle))
                {
                    if (this.equipmentServiceProxy != null)
                    {
                        //
                        // Hardware is available to this unit, run it there
                        //
                        DriverPowderXrd driver = new DriverPowderXrd(this.equipmentServiceProxy, this.configuration);
                        executionTime = driver.GetExecutionTime(this);
                    }
                    //else
                    //{
                    //    //
                    //    // This unit does not have hardware available, run the simulation instead
                    //    //
                    //    DriverSimActivity driver = new DriverSimActivity(this.configuration);
                    //    executionTime = driver.GetExecutionTime(this);
                    //}
                }

                // Validate total execution time
                this.validation.ValidateTotalTime(executionTime);

                //
                // Specification is valid
                //
                validationReport.estRuntime = executionTime;
                validationReport.accepted = true;
            }
            catch (Exception ex)
            {
                validationReport.errorMessage = ex.Message;
                Logfile.WriteError(ex.Message);
            }

            string logMessage = STRLOG_Accepted + validationReport.accepted.ToString();
            if (validationReport.accepted == true)
            {
                logMessage += Logfile.STRLOG_Spacer + STRLOG_ExecutionTime + validationReport.estRuntime.ToString() + STRLOG_seconds;
            }

            Logfile.WriteCompleted(STRLOG_ClassName, STRLOG_MethodName, logMessage);

            return validationReport;
        }

    }
}
